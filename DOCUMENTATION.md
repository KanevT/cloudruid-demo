![alt](https://assets.jobs.bg/assets/logo/2020-04-26/b_c8a132aff8e806def7a2f32f6c883e20.png)

---

#  Groceries Shop - Cloudroid Demo 
>Groceries Shop is a Command Line Applications (CLI) which enables us to choose a deal that includes two types of promotion. The first type of promotion is "Two For Three" and allows us to take a third product free of charge when buying two products listed in the promotion. The second type of promotion that is included in the deal is "Buy 1 Get 1 for Half Price" and it allows us to buy every second product at a half price from the product which is included in the promotion. All products can be added manualy  to the shop assortment or to be removed.  After the execution of each deal, the total amount of all products is printed as a result of the console. After that the transaction is closed and it is not possible to charge products on it. In order to purchase new products, it is necessary to open a new deal.
---

---

> The data of the application is stored in a data structure which is created as new every time when we run the project.

---

## Installation

### Prerequisites
The following list of software should be installed:
- [IntelliJ IDEA](https://www.jetbrains.com/idea/)
- [Java SE Development Kit 11](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)

### Clone
- Clone the project using `https://gitlab.com/KanevT/cloudruid-demo.git`  

### Setup

- Open the project via IntelliJ IDEA

- Run the project:

- First, add product to the shop assortiment list using the command in the below example.
> Example: **AddProductToMarket apple 50**

- Create a deal with the promotions and selected products.
> Example: **CreateDeal DealName apple banana tomato potato**

- Type in command for adding product to the same deal.
> Example: **AddProductToDeal FirstDeal apple banana banana potato tomato banana potato**

- You can also type in command to print all deals.
> Example: **PrintDailyDeals**

- Or remove product from shop assortiment.
> Example: **RemoveProductFromMarket watermelon**

- With command "Exit" you will end the execution of the program.

## Technologies

- Java
- JUnit
- GitLab


## Created By 
* Todor Kanev - [GitLab](https://gitlab.com/KanevT)
