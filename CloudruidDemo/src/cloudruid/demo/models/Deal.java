package cloudruid.demo.models;

import java.util.List;

public interface Deal {

    String getName();

    public double getSum();

    public void setSum(double sum);

    List<String> getTwoForTreeList();

    String getProductOfHalfPrice();

     void addProductToTwoForTreeList(String str);



}
