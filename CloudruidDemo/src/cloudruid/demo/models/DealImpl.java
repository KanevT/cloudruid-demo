package cloudruid.demo.models;

import cloudruid.demo.commands.Constants;
import cloudruid.demo.utils.ValidationHelper;

import java.util.ArrayList;
import java.util.List;

public class DealImpl implements Deal {

    private List<String> twoForTreeList;
    private String productOfHalfPrice;
    private String name;
    private double sum;

    public DealImpl(String name, List<String> twoForTreeList, String productOfHalfPrice) {
        setName(name);
        setProductOfHalfPrice(productOfHalfPrice);
        setTwoForTreeList(twoForTreeList);
        setSum(0);
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        ValidationHelper.checkNegativeNumber(sum);
        this.sum = sum;
    }

    @Override
    public void addProductToTwoForTreeList(String str) {
        this.twoForTreeList.add(str);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<String> getTwoForTreeList() {
        return new ArrayList<>(twoForTreeList);
    }

    @Override
    public String getProductOfHalfPrice() {
        return productOfHalfPrice;
    }

    @Override
    public String toString() {
        String products = convertToString(twoForTreeList);
        StringBuilder builder = new StringBuilder();
        builder.append(String.format(Constants.DEAL_INFORMATION,
                name, products, productOfHalfPrice, sum, Constants.JOIN_DELIMITER));

        return builder.toString();
    }

    private void setProductOfHalfPrice(String str) {
        ValidationHelper.checkNull(str, Constants.PRODUCT_HALF_PRICE_ERROR_MESSAGE);
        this.productOfHalfPrice = str;
    }

    private void setName(String name) {
        ValidationHelper.checkStringLength(name, Constants.MIN_NAME_LENGTH,
                Constants.MAX_NAME_LENGTH, Constants.DEAL_NAME_ERROR_MESSAGE);
        this.name = name;
    }

    private String convertToString(List<String> twoForTree) {
        StringBuilder builder = new StringBuilder();
        for (String element : twoForTree) {
            builder.append(element + " ");
        }
        return builder.toString();
    }

    private void setTwoForTreeList(List<String> products) {
        this.twoForTreeList = new ArrayList<>(products);
    }
}
