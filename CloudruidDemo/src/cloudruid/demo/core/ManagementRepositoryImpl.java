package cloudruid.demo.core;

import cloudruid.demo.commands.Constants;
import cloudruid.demo.core.contracts.ManagementRepository;
import cloudruid.demo.models.Deal;
import cloudruid.demo.utils.ValidationHelper;
import java.util.HashMap;
import java.util.Map;

public class ManagementRepositoryImpl implements ManagementRepository {

    private final Map<String, Deal> deals;
    private final Map<String, Double> products;

    public ManagementRepositoryImpl() {
        this.deals = new HashMap<>();
        this.products = new HashMap<>();
    }


    @Override
    public void addDeal(String name, Deal deal) {
        deals.put(name, deal);
    }

    @Override
    public Map<String, Deal> getAllDeals() {
        return new HashMap<>(deals);
    }

    @Override
    public Map<String, Double> getProducts() {
        return new HashMap<>(products);
    }

    @Override
    public Deal getDealByName(String name) {
        Deal deal = getAllDeals().get(name);
        ValidationHelper.checkNull(deal, String.format(Constants.DEAL_NOT_FOUND_MESSAGE,name));
         return deal;
    }

    @Override
    public void addProductToMarket(String name, double price) {
        products.put(name,price);
    }

    @Override
    public void removeProductFromMarket(String name) {
        products.remove(name);
    }
}
