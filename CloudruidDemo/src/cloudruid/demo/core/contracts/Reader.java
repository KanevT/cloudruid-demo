package cloudruid.demo.core.contracts;

public interface Reader {

    String readLine();

}
