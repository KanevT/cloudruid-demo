package cloudruid.demo.core.contracts;

import cloudruid.demo.commands.contracts.Command;

public interface CommandFactory {

    Command createCommand(String commandTypeAsString, ManagementFactory factory, ManagementRepository agencyRepository);

}
