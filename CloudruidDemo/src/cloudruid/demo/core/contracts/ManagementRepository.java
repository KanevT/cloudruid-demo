package cloudruid.demo.core.contracts;

import cloudruid.demo.models.Deal;

import java.util.Map;

public interface ManagementRepository {

    void addDeal(String name, Deal deal);

    Map<String, Deal> getAllDeals();

    Map<String, Double> getProducts();

    Deal getDealByName(String name);

    void addProductToMarket(String name, double price);

    void removeProductFromMarket(String name);
}
