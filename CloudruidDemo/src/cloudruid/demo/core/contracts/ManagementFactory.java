package cloudruid.demo.core.contracts;

import cloudruid.demo.models.DealImpl;
import java.util.List;

public interface ManagementFactory {

    DealImpl createDeal(String name, List<String> twoForTree, String buyOneGetOneHalfPrice);

}
