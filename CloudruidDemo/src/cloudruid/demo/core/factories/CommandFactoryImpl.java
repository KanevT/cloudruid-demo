package cloudruid.demo.core.factories;

import cloudruid.demo.commands.Constants;
import cloudruid.demo.commands.creations.*;
import cloudruid.demo.core.contracts.CommandFactory;
import cloudruid.demo.core.contracts.ManagementFactory;
import cloudruid.demo.core.contracts.ManagementRepository;
import cloudruid.demo.commands.contracts.Command;
import cloudruid.demo.commands.enums.CommandType;

public class CommandFactoryImpl implements CommandFactory {

    @Override
    public Command createCommand(String commandTypeAsString, ManagementFactory managementFactory, ManagementRepository managementRepository) {
        CommandType commandType = CommandType.valueOf(commandTypeAsString.toUpperCase());

        switch (commandType) {
            case CREATEDEAL:
                return new CreateDeal(managementFactory, managementRepository);
            case ADDPRODUCTTODEAL:
                return new AddProductToDeal(managementRepository);
            case PRINTDAILYDEALS:
                return new PrintDailyDeals(managementRepository);
            case ADDPRODUCTTOMARKET:
                return new AddProductToMarket(managementRepository);
            case REMOVEPRODUCTFROMMARKET:
                return new RemoveProductFromMarket(managementRepository);
        }

        throw new IllegalArgumentException(String.format(Constants.INVALID_COMMAND_ERROR_MESSAGE, commandTypeAsString));
    }
}
