package cloudruid.demo.core.factories;
import cloudruid.demo.core.contracts.ManagementFactory;
import cloudruid.demo.models.DealImpl;

import java.util.List;

public class ManagementFactoryImpl implements ManagementFactory {

    public DealImpl createDeal(String name, List<String> twoForTree, String buyOneGetOneHalfPrice){
        return new DealImpl(name, twoForTree, buyOneGetOneHalfPrice);
    }

}
