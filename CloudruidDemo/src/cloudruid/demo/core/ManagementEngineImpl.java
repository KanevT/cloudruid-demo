package cloudruid.demo.core;
import cloudruid.demo.core.contracts.*;
import cloudruid.demo.core.factories.ManagementFactoryImpl;
import cloudruid.demo.core.providers.CommandParserImpl;
import cloudruid.demo.commands.contracts.Command;
import cloudruid.demo.core.factories.CommandFactoryImpl;
import cloudruid.demo.core.providers.ConsoleReader;
import cloudruid.demo.core.providers.ConsoleWriter;
import java.util.List;

public class ManagementEngineImpl implements Engine {

    private static final String TERMINATION_COMMAND = "Exit";
    private static final String ERROR_EMPTY_COMMAND = "Command cannot be null or empty.";

    private final ManagementFactory managementFactory;
    private final CommandParser commandParser;
    private final ManagementRepository managementRepository;
    private final Writer writer;
    private final Reader reader;
    private final CommandFactory commandFactory;

    public ManagementEngineImpl() {
        managementFactory = new ManagementFactoryImpl();
        commandParser = new CommandParserImpl();
        writer = new ConsoleWriter();
        reader = new ConsoleReader();
        commandFactory = new CommandFactoryImpl();
        managementRepository = new ManagementRepositoryImpl();

    }

    @Override
    public void start() {
        while (true) {
            try {
                String commandAsString = reader.readLine();
                if (commandAsString.equalsIgnoreCase(TERMINATION_COMMAND)) {
                    break;
                }
                processCommand(commandAsString);

            } catch (Exception ex) {
                writer.writeLine(ex.getMessage() != null && !ex.getMessage().isEmpty() ? ex.getMessage() : ex.toString());
            }
        }
    }

    private void processCommand(String commandAsString) {
        if (commandAsString == null || commandAsString.trim().equals("")) {
            throw new IllegalArgumentException(ERROR_EMPTY_COMMAND);
        }

        String commandName = commandParser.parseCommand(commandAsString);
        Command command = commandFactory.createCommand(commandName, managementFactory, managementRepository);
        List<String> parameters = commandParser.parseParameters(commandAsString);
        String executionResult = command.execute(parameters);
        writer.writeLine(executionResult);
    }
}
