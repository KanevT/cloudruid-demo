package cloudruid.demo;

import cloudruid.demo.core.ManagementEngineImpl;

public class Startup {

    public static void main(String[] args) {
        ManagementEngineImpl engine = new ManagementEngineImpl();
        engine.start();
    }
}
