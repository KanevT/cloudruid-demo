package cloudruid.demo.commands.enums;

public enum CommandType {
    CREATEDEAL,
    PRINTDAILYDEALS,
    ADDPRODUCTTODEAL,
    REMOVEPRODUCTFROMMARKET,
    ADDPRODUCTTOMARKET;
}
