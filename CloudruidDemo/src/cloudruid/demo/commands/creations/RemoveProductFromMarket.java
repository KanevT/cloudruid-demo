package cloudruid.demo.commands.creations;

import cloudruid.demo.commands.Constants;
import cloudruid.demo.commands.contracts.Command;
import cloudruid.demo.core.contracts.ManagementRepository;
import cloudruid.demo.utils.ValidationHelper;

import java.util.List;

public class RemoveProductFromMarket implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;
    private final ManagementRepository managementRepository;

    public RemoveProductFromMarket(ManagementRepository managementRepository) {
        this.managementRepository = managementRepository;
    }

    @Override
    public String execute(List<String> parameters) {

        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(
                    Constants.INVALID_NUMBER_OF_ARGUMENTS, CORRECT_NUMBER_OF_ARGUMENTS, parameters.size()));
        }

        String productName = parameters.get(0).toLowerCase();

        ValidationHelper.checkProductAvailabilityInMarket(productName, managementRepository);
        managementRepository.removeProductFromMarket(productName);

        return String.format(Constants.PRODUCT_REMOVE_SUCCESSFUL_MESSAGE, productName);
    }
}
