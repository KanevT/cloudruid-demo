package cloudruid.demo.commands.creations;

import cloudruid.demo.commands.Constants;
import cloudruid.demo.commands.contracts.Command;
import cloudruid.demo.core.contracts.ManagementFactory;
import cloudruid.demo.core.contracts.ManagementRepository;
import cloudruid.demo.models.Deal;

import java.util.ArrayList;
import java.util.List;

public class CreateDeal implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 5;

    private final ManagementRepository managementRepository;
    private final ManagementFactory managementFactory;

    public CreateDeal(ManagementFactory managementFactory, ManagementRepository managementRepository) {
        this.managementRepository = managementRepository;
        this.managementFactory = managementFactory;
    }

    @Override
    public String execute(List<String> parameters) {

        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(
                    Constants.INVALID_NUMBER_OF_ARGUMENTS, CORRECT_NUMBER_OF_ARGUMENTS, parameters.size()));
        }

        String dealName = parameters.get(0);
        String productOne = parameters.get(1);
        String productTwo = parameters.get(2);
        String productTree = parameters.get(3);
        String productFour = parameters.get(4);

        List<String> twoForTreeProducts = new ArrayList<>(List.of(productOne, productTwo, productTree));

        if (managementRepository.getAllDeals().containsKey(dealName)) {
            throw new IllegalArgumentException(String.format(Constants.DEAL_EXIST_ERROR_MESSAGE, dealName));
        }

        Deal deal = managementFactory.createDeal(dealName, twoForTreeProducts, productFour);
        managementRepository.addDeal(dealName, deal);

        return String.format(Constants.DEAL_CREATE_SUCCESS_MESSAGE, dealName);
    }
}
