package cloudruid.demo.commands.creations;

import cloudruid.demo.commands.Constants;
import cloudruid.demo.commands.contracts.Command;
import cloudruid.demo.core.contracts.ManagementRepository;
import cloudruid.demo.models.Deal;
import cloudruid.demo.utils.ValidationHelper;

import java.util.ArrayList;
import java.util.List;

public class AddProductToDeal implements Command {

    private final ManagementRepository managementRepository;

    public AddProductToDeal(ManagementRepository managementRepository) {
        this.managementRepository = managementRepository;
    }

    @Override
    public String execute(List<String> parameters) {

        String dealName = parameters.get(0);
        Deal deal = managementRepository.getDealByName(dealName);

        checkIsDealClosed(deal);
        List<String> products = getProductList(parameters);

        double sumOfProducts = calculateSumOfProducts(products, deal);
        deal.setSum(sumOfProducts);

        return String.format(Constants.SUM_OF_DEAL_PRODUCT, dealName, sumOfProducts);
    }

    private void checkIsDealClosed(Deal deal) {
        if (deal.getSum() != 0) {
            throw new IllegalStateException(Constants.DEAL_CLOSED_ERROR_MESSAGE);
        }
    }

    private List<String> getProductList(List<String> parameters) {
        List<String> products = new ArrayList<>();
        for (int i = 1; i < parameters.size(); i++) {
            products.add(parameters.get(i).toLowerCase());
        }
        return products;
    }

    private double calculateSumOfProducts(List<String> products, Deal deal) {
        List<String> listDealProduct = deal.getTwoForTreeList();
        int countTwoForTree = Constants.ZERO;
        int countSecondIsHalfPrice = Constants.ZERO;
        double sum = Constants.ZERO;

        for (int i = 0; i < products.size(); i++) {
            String currentProduct = products.get(i).toLowerCase();

            ValidationHelper.checkProductAvailabilityInMarket(currentProduct, managementRepository);

            if (listDealProduct.contains(currentProduct.toLowerCase())) {
                countTwoForTree++;

                if (countTwoForTree % Constants.EVERY_THIRD_PRODUCT != Constants.ZERO) {
                    sum += getProductPrice(currentProduct);
                }

            } else if (currentProduct.equalsIgnoreCase(deal.getProductOfHalfPrice())) {
                countSecondIsHalfPrice++;

                if (countSecondIsHalfPrice % Constants.EVERY_SECOND_PRODUCT != Constants.ZERO) {
                    sum += getProductPrice(currentProduct);
                } else {
                    sum += getProductPrice(currentProduct) / Constants.DIVIDE_BY_TWO;
                }
            }
        }
        return convertToAws(sum);
    }

    private double getProductPrice(String productName) {
        return managementRepository.
                getProducts().get(productName.toLowerCase());
    }

    private double convertToAws(double sum) {
        return sum / Constants.CURRENCY_CONVERT_VALUE;
    }

}
