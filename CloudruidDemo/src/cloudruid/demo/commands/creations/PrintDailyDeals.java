package cloudruid.demo.commands.creations;

import cloudruid.demo.commands.Constants;
import cloudruid.demo.commands.contracts.Command;
import cloudruid.demo.core.contracts.ManagementRepository;
import cloudruid.demo.models.Deal;

import java.util.List;

public class PrintDailyDeals implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 0;
    private final ManagementRepository managementRepository;

    public PrintDailyDeals(ManagementRepository managementRepository) {
        this.managementRepository = managementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(
                    Constants.INVALID_NUMBER_OF_ARGUMENTS, CORRECT_NUMBER_OF_ARGUMENTS, parameters.size()));
        }

        StringBuilder builder = new StringBuilder();
        double totalSum = 0;

        for (Deal deal : managementRepository.getAllDeals().values()) {
            builder.append(deal.toString());
            totalSum += deal.getSum();
        }

        builder.append(String.format(Constants.TOTAL_DAILY_CASH_TURNOVER_MESSAGE, totalSum));

        return builder.toString();
    }
}
