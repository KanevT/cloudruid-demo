package cloudruid.demo.commands.creations;

import cloudruid.demo.commands.Constants;
import cloudruid.demo.commands.contracts.Command;
import cloudruid.demo.core.contracts.ManagementRepository;
import cloudruid.demo.utils.ValidationHelper;

import java.util.List;

public class AddProductToMarket implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

    private ManagementRepository managementRepository;

    public AddProductToMarket(ManagementRepository managementRepository) {
        this.managementRepository = managementRepository;
    }

    @Override
    public String execute(List<String> parameters) {

        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(
                    Constants.INVALID_NUMBER_OF_ARGUMENTS, CORRECT_NUMBER_OF_ARGUMENTS, parameters.size()));
        }

        String productName = parameters.get(0).toLowerCase();
        double productPrice = Double.parseDouble(parameters.get(1));

        ValidationHelper.checkStringLength(productName, Constants.MIN_NAME_LENGTH,
                Constants.MAX_NAME_LENGTH, Constants.PRODUCT_NAME_LENGTH_ERROR_MESSAGE);

        ValidationHelper.checkNegativeNumber(productPrice);
        managementRepository.addProductToMarket(productName, productPrice);

        return String.format(Constants.PRODUCT_ADDED_SUCCESSFUL_MESSAGE, productName);
    }
}
