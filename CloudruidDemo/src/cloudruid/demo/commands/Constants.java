package cloudruid.demo.commands;

public class Constants {

    // Error messages
    public static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments. Expected: %d, Received: %d";
    public static final String PRODUCT_NAME_LENGTH_ERROR_MESSAGE = "Product name must be between 2 and 15 symbols!";
    public static final String DEAL_CLOSED_ERROR_MESSAGE = "Deal already is closed! Please create new Deal!";
    public static final String PRODUCT_HALF_PRICE_ERROR_MESSAGE = "Product for half price cannot be null!";
    public static final String DEAL_NAME_ERROR_MESSAGE = "Deal name must be between 2 and 15 symbols.";
    public static final String POSITIVE_NUMBER_ERROR_MESSAGE = "The price must be positive number";
    public static final String INVALID_COMMAND_ERROR_MESSAGE = "Invalid command name: %s";
    public static final String DEAL_NOT_FOUND_MESSAGE = "Deal with name %s not found!";
    public static final String NAME_CANNOT_BE_NULL_MESSAGE = "Name cannot be null!";
    public static final String PRODUCT_NOT_FOUND_MESSAGE = "Product not found in the market!";
    public static final String JOIN_DELIMITER = "####################\n";

    // Command Success messages
    public static final String SUM_OF_DEAL_PRODUCT = "Total price of all products for deal with name %s is %.2f aws\n";
    public static final String PRODUCT_REMOVE_SUCCESSFUL_MESSAGE = "Product %s was remove successful!";
    public static final String PRODUCT_ADDED_SUCCESSFUL_MESSAGE = "Product %s was added successful!";
    public static final String DEAL_CREATE_SUCCESS_MESSAGE = "Deal with name %s was created";

    // DealImpl messages
    public static final String DEAL_INFORMATION = "Deal name : %s%nInput deals \"Two For Tree\" about products ( %s).%n" +
            "Input deal \"Buy One Get One Half Price\" about product (%s).%nTotal deal price: %.2f aws%n %s";
    public static final String TOTAL_DAILY_CASH_TURNOVER_MESSAGE = "Total daily cash turnover: %.2f aws";
    public static final String DEAL_EXIST_ERROR_MESSAGE = "Deal with name %s already exist!";


    // ==================================================
    public static final int CURRENCY_CONVERT_VALUE = 100;
    public static final int EVERY_SECOND_PRODUCT = 2;
    public static final int EVERY_THIRD_PRODUCT = 3;
    public static final int MAX_NAME_LENGTH = 15;
    public static final int MIN_NAME_LENGTH = 2;
    public static final int DIVIDE_BY_TWO = 2;
    public static final int ZERO = 0;

}
