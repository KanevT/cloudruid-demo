package cloudruid.demo.utils;

import cloudruid.demo.commands.Constants;
import cloudruid.demo.core.contracts.ManagementRepository;

public class ValidationHelper {


    public static void checkNull(Object obj, String message) {
        if (obj == null) {
            throw new NullPointerException(message);
        }
    }

    public static void checkStringLength(String str, int min, int max, String message) {
        checkNull(str, Constants.NAME_CANNOT_BE_NULL_MESSAGE);
        if (str.trim().length() <= min || str.trim().length() > max) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void checkNegativeNumber(double sum) {
        if (sum < 0) {
            throw new IllegalArgumentException(Constants.POSITIVE_NUMBER_ERROR_MESSAGE);
        }
    }

    public static void checkProductAvailabilityInMarket(String productName, ManagementRepository repository) {
        if (!repository.getProducts().containsKey(productName)) {
            throw new IllegalArgumentException(Constants.PRODUCT_NOT_FOUND_MESSAGE);
        }
    }
}
