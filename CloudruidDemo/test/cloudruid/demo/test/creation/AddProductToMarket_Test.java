package cloudruid.demo.test.creation;

import cloudruid.demo.commands.contracts.Command;
import cloudruid.demo.commands.creations.AddProductToMarket;
import cloudruid.demo.core.ManagementRepositoryImpl;
import cloudruid.demo.core.contracts.ManagementRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class AddProductToMarket_Test {

    private ManagementRepository repository;
    private List<String> parameters;
    private Command testCommand;

    @BeforeEach
    public void before() {
        repository = new ManagementRepositoryImpl();
        testCommand = new AddProductToMarket(repository);
    }


    @Test
    public void execute_Should_ThrowException_When_InvalidNumberOfParameter() {
        // Arrange
        parameters = Collections.emptyList();

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void execute_Should_ThrowException_When_ProductNameIsTooShort() {
        // Arrange
        parameters = Arrays.asList("io", "50");

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void execute_Should_ThrowException_When_ProductPriceIsNegative() {
        // Arrange
        parameters = Arrays.asList("apple", "-50");

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void execute_Should_AddProduct_When_ProductIsWithValidParameter() {
        // Arrange
        parameters = Arrays.asList("apple", "50");

        // Act
        testCommand.execute(parameters);

        // Assert
        Assertions.assertTrue( repository.getProducts().containsKey("apple"));
        Assertions.assertEquals(50, repository.getProducts().get("apple"));
    }


}
