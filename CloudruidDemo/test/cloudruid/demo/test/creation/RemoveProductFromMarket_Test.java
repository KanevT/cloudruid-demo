package cloudruid.demo.test.creation;

import cloudruid.demo.commands.contracts.Command;
import cloudruid.demo.commands.creations.RemoveProductFromMarket;
import cloudruid.demo.core.ManagementRepositoryImpl;
import cloudruid.demo.core.contracts.ManagementRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

public class RemoveProductFromMarket_Test {

    private ManagementRepository repository;
    private List<String> parameters;
    private Command testCommand;

    @BeforeEach
    public void before() {
        repository = new ManagementRepositoryImpl();
        testCommand = new RemoveProductFromMarket(repository);
        parameters = Collections.singletonList("apricot");
    }


    @Test
    public void execute_Should_ThrowException_When_InvalidNumberOfParameter() {
        // Arrange
        parameters = Collections.emptyList();

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void execute_Should_Throw_When_ProductNotAvailableInMarket() {
        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void execute_Should_RemoveProduct_When_ProductIsAvailableInMarket() {
        // Arrange
        repository.addProductToMarket("apricot", 50);

        // Act
        testCommand.execute(parameters);

        // Assert
        Assertions.assertFalse(repository.getProducts().containsKey("apple"));
    }



}
