package cloudruid.demo.test.creation;

import cloudruid.demo.commands.contracts.Command;
import cloudruid.demo.commands.creations.AddProductToDeal;
import cloudruid.demo.core.ManagementRepositoryImpl;
import cloudruid.demo.core.contracts.ManagementRepository;
import cloudruid.demo.models.Deal;
import cloudruid.demo.models.DealImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class AddProductToDeal_Test {

    private ManagementRepository repository;
    private List<String> parameters;
    private Command testCommand;
    private Deal testDeal;

    @BeforeEach
    public void before() {
        repository = new ManagementRepositoryImpl();
        testCommand = new AddProductToDeal(repository);
        testDeal = new DealImpl("testDeal", List.of("apple", "banana", "tomato"), "potato");
    }


    @Test
    public void checkIsDealClosed_Should_ThrowException_When_DealIsAlreadyClosed() {
        // Arrange
        parameters = Arrays.asList("testDeal", "apple", "banana", "banana", "potato", "tomato", "banana ", "potato");
        testDeal.setSum(1.99);
        repository.addDeal("testDeal", testDeal);

        // Act, Assert
        Assertions.assertThrows(IllegalStateException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void calculateSumOfProducts_Should_ReturnCorrectAnswer_When_ValidParameter() {
        // Arrange
        repository.addProductToMarket("apple", 50);
        repository.addProductToMarket("banana", 40);
        repository.addProductToMarket("tomato", 30);
        repository.addProductToMarket("potato",  26);
        parameters = Arrays.asList("testDeal", "apple", "banana", "banana", "potato", "tomato", "banana", "potato");
        repository.addDeal("testDeal", testDeal);

        // Act
        testCommand.execute(parameters);

        // Assert
        Assertions.assertEquals(repository.getDealByName("testDeal").getSum(), 1.99 );
    }

    @Test
    public void calculateSumOfProducts_Should_ThrowException_When_ProductNotFoundInMarket() {
        // Arrange
        parameters = Arrays.asList("testDeal", "apple", "banana", "banana");
        repository.addDeal("testDeal", testDeal);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

}
