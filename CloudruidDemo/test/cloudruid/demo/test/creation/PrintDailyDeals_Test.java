package cloudruid.demo.test.creation;

import cloudruid.demo.commands.contracts.Command;
import cloudruid.demo.commands.creations.PrintDailyDeals;
import cloudruid.demo.core.ManagementRepositoryImpl;
import cloudruid.demo.core.contracts.ManagementRepository;
import cloudruid.demo.models.Deal;
import cloudruid.demo.models.DealImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

public class PrintDailyDeals_Test {

    private ManagementRepository repository;
    private List<String> parameters;
    private Command testCommand;
    private Deal testDeal;

    @BeforeEach
    public void setUp() {
        repository = new ManagementRepositoryImpl();
        testCommand = new PrintDailyDeals(repository);
        testDeal = new DealImpl("testDeal", List.of("apple", "banana", "tomato"), "potato");
    }

    @Test
    public void execute_Should_ThrowException_When_InvalidNumberOfParameter() {
        // Arrange
        parameters = Collections.singletonList("Deal44");

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }


}
