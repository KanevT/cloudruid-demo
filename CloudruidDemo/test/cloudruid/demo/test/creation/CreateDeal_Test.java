package cloudruid.demo.test.creation;

import cloudruid.demo.commands.contracts.Command;
import cloudruid.demo.commands.creations.CreateDeal;
import cloudruid.demo.core.ManagementRepositoryImpl;
import cloudruid.demo.core.contracts.ManagementFactory;
import cloudruid.demo.core.contracts.ManagementRepository;
import cloudruid.demo.core.factories.ManagementFactoryImpl;
import cloudruid.demo.models.Deal;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.Arrays;
import java.util.List;

public class CreateDeal_Test {

    private ManagementRepository repository;
    private ManagementFactory factory;
    private List<String> parameters;
    private Command testCommand;

    @BeforeEach
    public void setUp(){
        repository = new ManagementRepositoryImpl();
        factory = new ManagementFactoryImpl();
        testCommand = new CreateDeal(factory, repository);
    }

    @Test
    public void execute_Should_ThrowException_When_InvalidNumberOfParameter() {
        // Arrange
        parameters = Arrays.asList("FirstDeal", "apple", "banana");

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void execute_Should_ThrowException_When_DealWithsSameNameExist() {
        // Arrange
        parameters = Arrays.asList("FirstDeal", "apple", "banana", "tomato", "potato");
        testCommand.execute(parameters);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void execute_Should_CreateDeal_When_ValidParameter() {
        // Arrange
        parameters = Arrays.asList("FirstDeal", "apple", "banana", "tomato", "potato");

        // Act
        testCommand.execute(parameters);
        Deal deal = repository.getDealByName("FirstDeal");

        // Assert
        Assertions.assertEquals(deal.getName(), "FirstDeal");
        Assertions.assertEquals(deal.getProductOfHalfPrice(), "potato");
        Assertions.assertEquals(deal.getSum(), 0);
    }
}
