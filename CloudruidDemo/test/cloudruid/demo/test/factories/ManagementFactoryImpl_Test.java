package cloudruid.demo.test.factories;


import cloudruid.demo.core.ManagementRepositoryImpl;
import cloudruid.demo.core.contracts.ManagementFactory;
import cloudruid.demo.core.contracts.ManagementRepository;
import cloudruid.demo.core.factories.ManagementFactoryImpl;
import cloudruid.demo.models.Deal;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

public class ManagementFactoryImpl_Test {

    private ManagementRepository repository;
    private ManagementFactory factory;
    private Deal testDeal;

    @BeforeEach
    private void setUp() {
        repository = new ManagementRepositoryImpl();
        factory = new ManagementFactoryImpl();
    }


    @Test
    public void createDeal_Should_CreateNewDeal_When_ValidParameter() {
        //Act
        Deal deal = factory.createDeal("testDeal",
                List.of("banana", "tomato", "potato"),
                "potato");

        //Assert
        Assertions.assertEquals("testDeal", deal.getName());

    }

}
