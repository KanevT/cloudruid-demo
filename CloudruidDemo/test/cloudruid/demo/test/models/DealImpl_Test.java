package cloudruid.demo.test.models;

import cloudruid.demo.models.Deal;
import cloudruid.demo.models.DealImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class DealImpl_Test {

    private Deal testDeal;
    private List<String> products;

    @BeforeEach
    public void setUp() {
        products = new ArrayList<>(List.of("banana", "tomato", "potato"));
        testDeal = new DealImpl("testDeal", products, "potato");
    }

    @Test
    public void constructor_Should_ThrowError_When_NameIsNull() {
        // Act, Assert
        Assertions.assertThrows(NullPointerException.class,
                () -> new DealImpl(null, products, "potato"));
    }

    @Test
    public void constructor_Should_ThrowError_When_NameLengthIsLessThanRequired() {
        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new DealImpl("t", products, "potato"));
    }

    @Test
    public void constructor_Should_ThrowError_When_NameLengthIsMoreThanRequired() {
        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new DealImpl("testDealTestDeal2", products, "potato"));
    }

    @Test
    public void constructor_Should_ThrowError_When_ProductHalfPriceIsNull() {
        // Act, Assert
        Assertions.assertThrows(NullPointerException.class,
                () -> new DealImpl("testDeal", products, null));
    }

    @Test
    public void constructor_Should_ThrowError_When_ListProductTwoForTreeIsNull() {
        // Act, Assert
        Assertions.assertThrows(NullPointerException.class,
                () -> new DealImpl("testDeal", null, "potato"));
    }

    @Test
    public void getTwoForTree_Should_ReturnShallowCopy() {
        // Arrange
        Deal deal = new DealImpl("testDeal", products, "potato");

        // Act
        List<String> supposedShallowCopy = deal.getTwoForTreeList();
        deal.addProductToTwoForTreeList("watermelon");

        // Assert
        Assertions.assertEquals(3, supposedShallowCopy.size());
    }

    @Test
    public void setSum_Should_ThrowError_When_SumIsNegativeNumber() {
        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testDeal.setSum(-6.58));
    }


}
